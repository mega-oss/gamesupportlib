package com.mega.games.support.analytics

import com.mega.games.support.MegaServices

class FpsAnalytics(private val services: MegaServices) {
    //for getting average fps
    private var framesElapsed = 0
    private var elapsedTime = 0f

    //for getting mn max
    private var minFPS = Float.MAX_VALUE
    private var maxFPS = 0f
    private var minMaxFramesElapsed = 0
    private var minMaxTimeElapsed = 0f
    private val minMaxCaptureTimeWindow = 0.25f

    private val fpsEventLogWindow: Float = try {
        services.config()["_fps_event_log_window"].toString().toFloat()
    } catch (t:Throwable) {
        15f
    }

    fun logFpsEvents(dt: Float){
        framesElapsed++
        elapsedTime += dt

        minMaxFramesElapsed++
        minMaxTimeElapsed += dt

        if(minMaxTimeElapsed > minMaxCaptureTimeWindow) {
            val fps = minMaxFramesElapsed / minMaxTimeElapsed
            if (minFPS > fps) {
                minFPS = fps
            }

            if (maxFPS < fps) {
                maxFPS = fps
            }

            minMaxTimeElapsed = 0f
            minMaxFramesElapsed = 0
        }

        if(elapsedTime > fpsEventLogWindow){
            val data: HashMap<String, Float> = HashMap()

            data["minFPS"] = minFPS
            data["maxFPS"] = maxFPS
            data["avgFPS"] = framesElapsed/elapsedTime

            elapsedTime = 0f
            maxFPS = 0f
            minFPS = Float.MAX_VALUE
            framesElapsed = 0

            services.analytics().logEvent("fpsData", data)
        }
    }
}