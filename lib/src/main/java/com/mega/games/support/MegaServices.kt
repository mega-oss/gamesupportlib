package com.mega.games.support

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.loaders.FileHandleResolver
import com.badlogic.gdx.assets.loaders.resolvers.ClasspathFileHandleResolver
import com.badlogic.gdx.assets.loaders.resolvers.ResolutionFileResolver

open class MegaServices(private val config: GameConfig = mapOf()) {
    private val analytics by lazy {
        object : MegaAnalytics {
            override fun logEvent(event: String, data: Map<String, Any>?) {
                Gdx.app.log("GameAnalytics", "$event data = $data")
            }
        }
    }
    private val callbacks = MegaCallbacks()
    private var showTutorial = true

    private val baseFileResolver = ResolutionFileResolver(
        ClasspathFileHandleResolver(),
        ResolutionFileResolver.Resolution(640, 480, "640x480"),
        ResolutionFileResolver.Resolution(1280, 720, "1280x720"),
        ResolutionFileResolver.Resolution(1920, 1080, "1920x1080")
    )

    private val fileResolver = FileHandleResolver { fileName ->
        if (fileName.startsWith(SHARED_ASSETS_PREFIX)) {
            Gdx.files.classpath(fileName)
        } else {
            baseFileResolver.resolve(fileName)
        }
    }
    private var highScore: Long = 0

    init {
        //production should extend this and save tutCompleted info & submit scores
        callbacks.addListener(object : MegaCallbacks.Listener {
            override fun onEvent(event: MegaCallbacks.Event, data: Any?) {
                when (event) {
                    MegaCallbacks.Event.TUTORIAL_COMPLETED -> {
                        showTutorial = false
                    }

                    MegaCallbacks.Event.GAME_OVER -> {
                        if (data != null && data is Long && data > highScore) {
                            highScore = data
                        }
                    }
                }
            }

        })
    }

    open fun analytics(): MegaAnalytics = analytics
    open fun showTutorial(): Boolean = showTutorial
    open fun callbacks(): MegaCallbacks = callbacks
    open fun config(): GameConfig = config
    open fun fileResolver(): FileHandleResolver = fileResolver
    open fun getHighScore(): Long = highScore

    companion object {
        const val SHARED_ASSETS_PREFIX = "mega.games"
        const val FONT_ROBOTO_REGULAR = "$SHARED_ASSETS_PREFIX/fonts/Roboto-Regular.ttf"
        const val FONT_ROBOTO_BOLD = "$SHARED_ASSETS_PREFIX/fonts/Roboto-Bold.ttf"
        const val FONT_ROBOTO_LIGHT = "$SHARED_ASSETS_PREFIX/fonts/Roboto-Light.ttf"
        const val FONT_ROBOTOCONDENSED_REGULAR = "$SHARED_ASSETS_PREFIX/fonts/RobotoCondensed-Regular.ttf"
        const val FONT_ROBOTOCONDENSED_BOLD = "$SHARED_ASSETS_PREFIX/fonts/RobotoCondensed-Bold.ttf"
        const val FONT_ROBOTOCONDENSED_LIGHT = "$SHARED_ASSETS_PREFIX/fonts/RobotoCondensed-Light.ttf"
        const val FONT_GOTHAMROUNDED_MEDIUM = "$SHARED_ASSETS_PREFIX/fonts/GothamRounded-Medium.ttf"
        const val FONT_GOTHAMROUNDED_BOLD = "$SHARED_ASSETS_PREFIX/fonts/GothamRounded-Bold.ttf"
    }
}