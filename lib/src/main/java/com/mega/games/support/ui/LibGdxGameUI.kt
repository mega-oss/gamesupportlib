package com.mega.games.support.ui

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputProcessor
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.NinePatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.ScreenViewport
import com.mega.games.support.MegaServices

open class LibGdxGameUI(private val services: MegaServices) : GameUIManager, ApplicationAdapter() {

    private var onPlayAgainListener: (() -> Unit)? = null
    private var onQuitListener: (() -> Unit)? = null

    private var prevInputProcessor: InputProcessor? = null

    private var scheduledScreen = Screen.NONE
    private var currentScreen = Screen.NONE
    private var score = 0L

    enum class Screen {
        NONE, GAME_OVER
    }

    private fun showUI(): Boolean {
        return currentScreen != Screen.NONE
    }

    private lateinit var stage: Stage
    private lateinit var titleFont: BitmapFont
    private lateinit var regularFont: BitmapFont
    private lateinit var titleLabelStyle: Label.LabelStyle
    private lateinit var regularLabelStyle: Label.LabelStyle
    private lateinit var buttonStyle: TextButton.TextButtonStyle
    private var tableBgTexture: Texture? = null

    override fun create() {
        //load fonts
        stage = Stage(ScreenViewport())

        val gen = FreeTypeFontGenerator(
            services.fileResolver().resolve(MegaServices.FONT_ROBOTO_REGULAR)
        )
        titleFont = gen.generateFont(FreeTypeFontGenerator.FreeTypeFontParameter().apply { size = 96 })
        regularFont = gen.generateFont(FreeTypeFontGenerator.FreeTypeFontParameter().apply { size = 48 })
        gen.dispose()

        titleLabelStyle = Label.LabelStyle(titleFont, Color.WHITE)
        regularLabelStyle = Label.LabelStyle(regularFont, Color.WHITE)

        //todo dispose the texture
        val btnPatchDrawable = NinePatchDrawable(
            NinePatch(
                Texture(services.fileResolver().resolve("${MegaServices.SHARED_ASSETS_PREFIX}/button.9.png")),
                1,
                1,
                1,
                1
            )
        )

        btnPatchDrawable.run {
            buttonStyle = TextButton.TextButtonStyle(this, this, this, regularFont)
        }
    }

    override fun render() {
        if (scheduledScreen != Screen.NONE) {
            //build scheduled screen
            if (scheduledScreen == Screen.GAME_OVER) {
                buildGameOverStage()
            }
            currentScreen = scheduledScreen
            scheduledScreen = Screen.NONE
        }

        if (showUI()) {
            stage.act(Gdx.graphics.deltaTime)
            stage.draw()
        }
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height, true)
    }

    override fun showGameOverScreen(score: Long) {
        services.analytics().logEvent("game_over", mapOf("score" to score))

        this.score = score
        scheduledScreen = Screen.GAME_OVER
    }

    override fun setOnPlayAgainListener(listener: () -> Unit) {
        onPlayAgainListener = listener
    }

    override fun setOnQuitListener(listener: () -> Unit) {
        onQuitListener = listener
    }

    private fun switchInputProcessor() {
        prevInputProcessor = Gdx.input.inputProcessor
        Gdx.input.inputProcessor = stage
    }

    private fun restoreInputProcessor() {
        Gdx.input.inputProcessor = prevInputProcessor
    }

    private fun buildGameOverStage() {
        Gdx.app.debug("libgdxGameUIManager", "building game over scene")
        setupStage { table ->

            table.add(Container<Label>(
                Label("Game Over", titleLabelStyle).apply {
                    setWrap(true)
                    setAlignment(Align.top)
                }
            ).apply {
                padBottom(100f)
            }).row()

            table.add(Container<Label>(
                Label("Score: $score", regularLabelStyle).apply {
                    setWrap(true)
                    setAlignment(Align.top)
                }
            ).apply {
                padBottom(50f)
            }).row()

            table.add(Container<Label>(
                Label("High Score: ${services.getHighScore()}", regularLabelStyle).apply {
                    setWrap(true)
                    setAlignment(Align.top)
                }
            ).apply {
                padBottom(200f)
            }).row()

            table.add(Container<TextButton>(
                TextButton("Restart", buttonStyle).apply {
                    padLeft(32f)
                    padRight(32f)
                    padTop(24f)
                    padBottom(24f)

                    addListener(object : ClickListener() {
                        override fun clicked(event: InputEvent?, x: Float, y: Float) {
                            restartGame()
                        }
                    })
                }
            ).apply {
                padBottom(50f)
            }).row()

            table.add(TextButton("Quit", buttonStyle).apply {
                padLeft(64f)
                padRight(64f)
                padTop(24f)
                padBottom(24f)

                addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        quitGame()
                    }
                })
            }).row()
        }
    }

    private fun setupStage(sceneMaker: (table: Table) -> Unit) {
        if (scheduledScreen != Screen.NONE) {
            stage.clear()

            val table = Table()
            table.setFillParent(true)

            val bgPixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888).apply {
                setColor(Color(0x000000cc))
                fill()
            }

            tableBgTexture = Texture(bgPixmap)
            bgPixmap.dispose()
            val textureRegionDrawableBg = TextureRegionDrawable(TextureRegion(tableBgTexture))

            table.background(textureRegionDrawableBg)

            sceneMaker(table)

            stage.addActor(table)
            switchInputProcessor()
        }
    }

    private fun tearDown() {
        restoreInputProcessor()
        stage.clear()
        currentScreen = Screen.NONE
        tableBgTexture?.dispose()
        tableBgTexture = null
        score = 0
    }

    private fun restartGame() {
        onPlayAgainListener?.let {
            tearDown()
            it()
        }
    }

    private fun quitGame() {
        tearDown()
        onQuitListener?.let { it() }
    }

    override fun dispose() {
        stage.dispose()
        titleFont.dispose()
        regularFont.dispose()
        tableBgTexture?.dispose()
    }
}