package com.mega.games.support

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.mega.games.support.analytics.FpsAnalytics
import com.mega.games.support.ui.GameUIManager

/**
 * The entrypoint of a game must extend this class
 *
 */
abstract class MegaGame(protected val services: MegaServices) : Game() {
    private lateinit var uiManager: GameUIManager
    private var isPlaying = false

    private val fpsAnalytics = FpsAnalytics(services)

    init {
        services.callbacks().addListener(object : MegaCallbacks.Listener {
            override fun onEvent(event: MegaCallbacks.Event, data: Any?) {
                when (event) {
                    MegaCallbacks.Event.GAME_OVER -> {
                        if (isPlaying) {
                            isPlaying = false
                            uiManager.showGameOverScreen(data!! as Long)
                        }
                    }
                    MegaCallbacks.Event.GAME_STARTED -> {
                        isPlaying = true
                    }
                    else -> {
                        //not interested in other events
                    }
                }
            }
        })
    }

    fun isPlaying() = isPlaying

    fun setGameUIManager(uiManager: GameUIManager) {
        this.uiManager = uiManager
    }

    override fun create() {
        if (uiManager is ApplicationListener) {
            (uiManager as ApplicationListener).create()
        }
    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)

        if (uiManager is ApplicationListener) {
            (uiManager as ApplicationListener).resize(width, height)
        }
    }

    override fun render() {
        super.render()
        if(isPlaying()) {
            fpsAnalytics.logFpsEvents(Gdx.graphics.deltaTime)
        }
        if (uiManager is ApplicationListener) {
            (uiManager as ApplicationListener).render()
        }
    }

    override fun pause() {
        super.pause()
        if (isPlaying) {
            forceGameOver()
        }
    }

    override fun resume() {
        super.resume()

        if (uiManager is ApplicationListener) {
            (uiManager as ApplicationListener).resume()
        }
    }

    override fun dispose() {
        super.dispose()

        if (uiManager is ApplicationListener) {
            (uiManager as ApplicationListener).dispose()
        }
    }

    /**
     * This is called when the game needs to stop.
     * The reasons could be: Pause | Play time over
     */
    abstract fun forceGameOver()

    /**
     * This is called when the player wants to start again.
     * The game must reset its state and start from zero
     */
    abstract fun restartGame()
}