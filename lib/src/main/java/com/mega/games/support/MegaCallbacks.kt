package com.mega.games.support

/**
 * These are the callback methods that a game must call
 * at appropriate points in the game's lifecycle.
 */
class MegaCallbacks {
    private val listeners = arrayListOf<Listener>()

    /**
     * This is called when game has loaded all its assets and ready to start (possibly rendering its landing screen)
     */
    fun loaded() {
        listeners.forEach { it.onEvent(Event.GAME_LOADED) }
    }

    /**
     * This is called when the game has finished the tutorial/how-to
     */
    fun tutorialCompleted() {
        listeners.forEach { it.onEvent(Event.TUTORIAL_COMPLETED) }
    }

    /**
     * This is called when user has started playing the game
     */
    fun playStarted() {
        listeners.forEach { it.onEvent(Event.GAME_STARTED) }
    }

    /**
     * This is called when gameplay finishes due to any reason like
     * death or the game's endGame method is called.
     */
    fun gameOver(score: Long) {
        listeners.forEach { it.onEvent(Event.GAME_OVER, score) }
    }

    fun addListener(listener: Listener) {
        listeners.add(listener)
    }

    interface Listener {
        fun onEvent(event: Event, data: Any? = null)
    }

    enum class Event {
        TUTORIAL_COMPLETED, GAME_LOADED, GAME_STARTED, GAME_OVER
    }
}