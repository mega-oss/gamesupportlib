package com.mega.games.support

interface MegaAnalytics {
    fun logEvent(event : String, data: Map<String, Any>? = null)
}