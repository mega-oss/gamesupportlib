package com.mega.games.support.ui

interface GameUIManager {
    fun showGameOverScreen(score: Long)

    fun setOnPlayAgainListener(listener: () -> Unit)
    fun setOnQuitListener(listener: () -> Unit)
}