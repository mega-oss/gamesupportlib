package com.mega.games.support

enum class GameEndReason {
    GAME_OVER,
    ROOM_DEADLINE_EXCEEDED,
    AUTHENTICATION_FAILURE
}